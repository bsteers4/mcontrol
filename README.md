## MControl, MPRIS media player controller

#### Written by Bruce Steers
<hr>

Features...

* Application creates a system tray icon.
* Finds all loaded MPRIS enabled media players running including Spotify, SMPlayer, VLC, Clementine, YouTube(Firefox)

* Play, Pause, Next/Prev track, FFwd RWnd, Set play position, Volume, mute.
* Toggle fullscreen, shuffle or loop mode

Additional features.
* Auto-stop other players playing when a new media is played.
* Open playing local file folder.
* Play Next/Prev files in folder with local urls.
